# space-invaderz

A very small space game in Vanilla Javascript under the AGPL licence

## How to install

Just download the source code and double click on index.html to open it in your web browser

## How to play

Use the arrows to move the ship and Enter to shoot.

*Made by Fla & Aqua*