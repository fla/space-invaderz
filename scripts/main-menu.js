class MainMenu {
  #app;
  difficultyDialog;

  // Container of the menu, add / remove the CSS class "hidden" to make it disappear.
  // This is done by the showMainMenu() / hideMainMenu() methods
  #mainMenuContainer = document.getElementById("main-menu");

  // Store in properties all the buttons of the menu
  #launchBtn = document.getElementById("launch-btn");
  #difficultyBtn = document.getElementById("difficulty-btn");

  constructor(app) {
    this.#app = app;
    this.difficultyDialog = new DifficultyDialog(app);
    this.initializeBtnEvents();
  }

  // Attach the click event to all the buttons for the menu
  initializeBtnEvents() {
    this.#launchBtn.addEventListener("click", () => this.onLaunchGame());
    this.#difficultyBtn.addEventListener("click", () => this.difficultyDialog.openDialog());
  }

  showMainMenu() {
    this.#mainMenuContainer.classList.remove("hidden");
  }

  hideMainMenu() {
    this.#mainMenuContainer.classList.add("hidden");
  }

  onLaunchGame() {
    this.#app.launchGame();
    this.hideMainMenu();
  }

  setDifficulty(selectedDifficulty) {
    document.getElementById("difficulty-label").textContent = selectedDifficulty;
  }
}