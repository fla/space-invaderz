class App {
    #game;
    music;

    #mainMenu;
    #ingameMenu;
    #globalBtns;

    constructor() {
        this.#game = new Game();
        this.music = new Music();
        this.#mainMenu = new MainMenu(this);
        this.#ingameMenu = new IngameMenu(this);
        this.#globalBtns = new GlobalButtons(this);

        this.setDifficulty("Easy"); // Initial difficulty is Easy
    }

    setDifficulty(difficulty) {
        this.#game.difficulty = difficulty;
        this.#mainMenu.setDifficulty(difficulty);
    }

    launchGame() {
        this.#globalBtns.showOpenIngameMenuButton();
        this.#game.launch();
        this.music.playGameMusic(true);
    }

    pauseGame() {
        this.#game.pause();
        this.#ingameMenu.openDialog();
        this.music.playMenuMusic(true);
    }

    resumeGame() {
        this.#game.resume();
        this.music.playGameMusic(true);
    }

    restartGame() {
        this.#game.restart();
        this.music.playGameMusic(true);
    }

    // TODO #6 add the "openOptions()" method here

    exitGame() {
        this.#game.initialize();
        this.#mainMenu.showMainMenu();
        this.#globalBtns.hideOpenIngameMenuButton();
    }
}