const CSS_CLASS_UNMUTED = "unmuted";

class GlobalButtons {
  #app;
    
  #openIngameMenuBtn = document.getElementById("open-ingame-menu-btn");
  #toggleSoundBtn = document.getElementById("toggle-sound-btn");

  constructor(app) {
    this.#app = app;
    this.initializeBtnEvents();
  }

  initializeBtnEvents() {
    this.#openIngameMenuBtn.addEventListener("click", () => this.onPauseGame());
    this.#toggleSoundBtn.addEventListener("click", () => this.onToggleSound());
  }

  onPauseGame() {
    this.#app.pauseGame();
  }

  showOpenIngameMenuButton() {
    this.#openIngameMenuBtn.classList.remove("hidden");
  }

  hideOpenIngameMenuButton() {
    this.#openIngameMenuBtn.classList.add("hidden");
  }

  onToggleSound() {
    if (this.#toggleSoundBtn.classList.contains(CSS_CLASS_UNMUTED)) {
        this.#toggleSoundBtn.classList.remove(CSS_CLASS_UNMUTED);
        this.#app.music.pauseMusic();
    } else {
        this.#toggleSoundBtn.classList.add(CSS_CLASS_UNMUTED);
        this.#app.music.resumeMusic();
    }
  }
}