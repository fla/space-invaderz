const REFRESH_RATE = 50;

class Game {
  difficulty;

  #nbLoops;
  #playing;

  constructor() {
    this.initialize();
  }

  initialize() {
    this.#nbLoops = 0;
    this.#playing = true;
  }

  launch() {
    console.debug("Game launched. Difficulty: " + this.difficulty);
    this.#gameLoop();
  }

  pause() {
    this.#playing = false;
  }

  resume() {
    this.#playing = true;
    this.#gameLoop();
  }

  restart() {
    this.initialize();
    this.#gameLoop();
  }

  #gameLoop() {
    this.#nbLoops = this.#nbLoops + 1;
    if ((REFRESH_RATE * this.#nbLoops) % 1000 === 0) {
      console.log((REFRESH_RATE * this.#nbLoops) / 1000 + "sec");
    }
    if (this.#playing) {
      setTimeout(() => this.#gameLoop(), REFRESH_RATE);
    }
  }
}