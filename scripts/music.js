class Music {
    isSoundEnabled = false;
    isCurrentMusicMenu = true;
    #menuMusic = document.getElementById("menu-music");
    #gameMusic = document.getElementById("game-music");

    resumeMusic() {
        this.isSoundEnabled = true;
        if (this.isCurrentMusicMenu) {
            this.playMenuMusic(false);
        } else {
            this.playGameMusic(false);
        }
    }

    pauseMusic() {
        this.isSoundEnabled = false;
        this.stopMenuMusic();
        this.stopGameMusic();
    }

    playMenuMusic(restart) {
        this.stopGameMusic();
        this.isCurrentMusicMenu = true;
        if (restart) {
            this.#menuMusic.load(); // Reset it to the beginning
        }
        if (this.isSoundEnabled) {
            this.#menuMusic.play();
        }
    }

    stopMenuMusic() {
        this.#menuMusic.pause();
    }

    playGameMusic(restart) {
        this.stopMenuMusic();
        this.isCurrentMusicMenu = false;
        if (restart) {
            this.#gameMusic.load(); // Reset it to the beginning
        }
        if (this.isSoundEnabled) {
            this.#gameMusic.play();
        }
    }

    stopGameMusic() {
        this.#gameMusic.pause();
    }
}