/**
 * Class representing a dialog window.
 * Initialized with a string containing the id of the HTML element which is the dialog.
 * This element also has to have the `dialog` CSS class
 */
class Dialog {

  dialog;
  onClose;

  constructor(dialogId) {
    this.dialog = document.getElementById(dialogId);
    this.onClose = this.closeDialog;

    this.dialog.addEventListener("click", (ev) => {
      if (ev.target === this.dialog) {
        // We are clicking on the dialog container itself, so in the background, not on the content
        this.onClose();
      }
    });
  }

  openDialog() {
    this.dialog.classList.remove("hidden");
  }

  closeDialog() {
    this.dialog.classList.add("hidden");
  }
}