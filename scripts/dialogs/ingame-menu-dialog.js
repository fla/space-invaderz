class IngameMenu extends Dialog {
  #app;

  #resumeBtn = document.getElementById("resume-btn");
  #restartBtn = document.getElementById("restart-btn");
  // TODO #6 add the option button property here
  #exitBtn = document.getElementById("exit-btn");

  constructor(app) {
    super("ingame-menu-dialog");
    this.onClose = this.onResumeGame;
    this.#app = app;
    this.initializeBtnEvents();
  }

  // Attach the click event to all the buttons for the menu
  initializeBtnEvents() {
    this.#resumeBtn.addEventListener("click", () => this.onResumeGame());
    this.#restartBtn.addEventListener("click", () => this.onRestartGame());
    // TODO #6 add click on the option button here
    this.#exitBtn.addEventListener("click", () => this.onExitGame(this));
  }

  onResumeGame() {
    this.#app.resumeGame();
    this.closeDialog();
  }

  onRestartGame() {
    this.#app.restartGame();
    this.closeDialog();
  }

  // TODO #6 add onOpenOptions here

  onExitGame() {
    this.#app.exitGame();
    this.closeDialog();
  }
}