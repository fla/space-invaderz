class DifficultyDialog extends Dialog {

  constructor(app) {
    super("difficulty-dialog");  // Match the `id` defined on the HTML element with the class `dialog`

    const btns = this.dialog.getElementsByTagName("button");
    for (let btn of btns) {
      btn.addEventListener("click", (ev) => {
        app.setDifficulty(ev.currentTarget.textContent);
        this.closeDialog();
      });
    }
  }
}